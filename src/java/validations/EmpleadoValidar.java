/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package validations;

import models.Empleado;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author Nelson
 */
public class EmpleadoValidar implements Validator{

    @Override
    public boolean supports(Class<?> type) {
        return Empleado.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Empleado empleado = (Empleado)o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nombre", "required.nombre","El campo nombre es obligatorio");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "apellidos", "required.apellidos","El campo apellidos es obligatorio");
    }
    
}
