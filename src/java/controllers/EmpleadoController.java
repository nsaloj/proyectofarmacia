/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import models.Empleado;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import utils.Conexion;
import validations.EmpleadoValidar;

@Controller
//@RequestMapping("/empleados")
public class EmpleadoController {

    private EmpleadoValidar empleado_validar;
    private HttpServletRequest request;
    private JdbcTemplate jdbct;
    private Conexion conexion;
    
    
    public EmpleadoController()
    {
        this.conexion = new Conexion();
        this.jdbct = new JdbcTemplate(conexion.getDataSource());        
        empleado_validar = new EmpleadoValidar();
    }

    @RequestMapping(value="nuevo-empleado.htm", method=RequestMethod.GET)
    public ModelAndView nuevoEmpleado() {
        ModelAndView mav = new ModelAndView("empleado/nuevo_empleado","command", new Empleado());
                
        String query = "SELECT * FROM PROYECTO.\"tb_puesto\"";
        List puestos = this.jdbct.queryForList(query);
        
        mav.addObject("puestos", puestos);
        
        return mav;
    }
    @RequestMapping(value="nuevo-empleado.htm",method=RequestMethod.POST)
    public ModelAndView nuevoEmpleado(
            @ModelAttribute("empleados") Empleado e,
            BindingResult result, 
            SessionStatus status
    ) {
        this.empleado_validar.validate(e, result);
        if(result.hasErrors())
        {
            System.out.println(result.getAllErrors());
            ModelAndView mav = new ModelAndView();
            mav.setViewName("empleado/nuevo_empleado");
            return mav;
        }
        else{
            this.jdbct.update("INSERT INTO PROYECTO.\"tb_empleado\" (\"NOMBRES\",\"APELLIDOS\",\"PUESTO_ID\") values (?,?,?)",e.getNombre(), e.getApellidos(), (e.getPuesto() != null)? e.getPuesto().getPuesto_id():null);
            return new ModelAndView("redirect:/empleados.htm");
        }
    }
    
    @RequestMapping("empleados.htm")
    public ModelAndView empleados() {
        ModelAndView mav = new ModelAndView("empleado/empleados","command",new Empleado());
        
        String query = "SELECT emp.Nombres , emp.Apellidos, p.Nombre as Puesto FROM PROYECTO.\"tb_empleado\" emp"
                + " INNER JOIN PROYECTO.\"tb_puesto\" p ON p.Puesto_id = emp.Puesto_id";
        
        List empleados = this.jdbct.queryForList(query);
        
        mav.addObject("empleados",empleados);
        return mav;
    }
}
