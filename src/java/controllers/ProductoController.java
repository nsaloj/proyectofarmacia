/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import utils.Conexion;

/**
 *
 * @author Nelson
 */
public class ProductoController {
    private HttpServletRequest request;
    private JdbcTemplate jdbct;
    private Conexion conexion;
    
    
    public ProductoController()
    {
        this.conexion = new Conexion();
        this.jdbct = new JdbcTemplate(conexion.getDataSource());        
    }
    

    @RequestMapping("nuevo-producto.htm")
    public ModelAndView nuevoProducto() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("producto/nuevo_producto");
        return mav;
    }
    
    @RequestMapping("inventario.htm")
    public ModelAndView productos() {
        ModelAndView mav = new ModelAndView();
                
        
        String query = "SELECT * FROM PROYECTO.\"tb_producto\"";
        List productos = this.jdbct.queryForList(query);
        
        mav.setViewName("producto/inventario");
        mav.addObject("productos",productos);
        return mav;
    }    
}
