/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import models.Empleado;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import utils.Conexion;

@Controller
public class PuestoController {

    private HttpServletRequest request;
    private JdbcTemplate jdbct;
    private Conexion conexion;
    
    
    public PuestoController()
    {
        this.conexion = new Conexion();
        this.jdbct = new JdbcTemplate(conexion.getDataSource());        
    }
    

    @RequestMapping("nuevo-puesto.htm")
    public ModelAndView nuevoPuesto() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("puesto/nuevo_puesto");
        return mav;
    }
    
    @RequestMapping("puestos.htm")
    public ModelAndView puestos() {
        ModelAndView mav = new ModelAndView();
                
        
        String query = "SELECT * FROM PROYECTO.\"tb_puesto\"";
        List puestos = this.jdbct.queryForList(query);
        
        mav.setViewName("puesto/puestos");
        mav.addObject("puestos",puestos);
        return mav;
    }    
}
