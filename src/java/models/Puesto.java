/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

public class Puesto {
    private int Puesto_id;
    private String Nombre;

    public Puesto() {
    }
    
    public Puesto(int Puesto_id, String Nombre) {
        this.Puesto_id = Puesto_id;
        this.Nombre = Nombre;
    }

    public int getPuesto_id() {
        return Puesto_id;
    }

    public void setPuesto_id(int Puesto_id) {
        this.Puesto_id = Puesto_id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }
    
}
