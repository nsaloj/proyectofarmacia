/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 *
 * @author Nelson
 */
public class Conexion {
    
    private String host = "localhost";
    private String user = "proyecto";
    private String pass = "12345";
    
    private Connection conexion =null;
    public Connection conectar(){
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            conexion=DriverManager.getConnection("jdbc:oracle:thin:@//"+host+":1521/XE",user,pass);
            return conexion;
        } catch (ClassNotFoundException|SQLException ex) {            
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public Connection getConexion()
    {
        return this.conexion;
    }
    public void desconectar() throws SQLException
    {
        conexion.close();
    }
    
     public static void main(String[] args) {
         Conexion conexion = new Conexion();
         conexion.conectar();
     }
     
     public DriverManagerDataSource getDataSource(){
         DriverManagerDataSource dmds = new DriverManagerDataSource();
         dmds.setDriverClassName("oracle.jdbc.driver.OracleDriver");
         dmds.setUrl("jdbc:oracle:thin:@//"+host+"/XE");
         dmds.setUsername(user);
         dmds.setPassword(pass);
         return dmds;
     }
}
