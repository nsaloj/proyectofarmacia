<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Empleados</title>
        <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="<c:url value="/assets/css/Proyecto.css" />" rel="stylesheet">
    </head>
    <body>
        <c:import url="../navbar/navbar.jsp" />
        <div class="container container-top">
            <div class="row">
                <h1>Empleados</h1>
                <div>
                    <a href="<c:url value="nuevo-empleado.htm"/>" class="btn btn-success">Crear empleado <i class="glyphicon glyphicon-plus"></i></a>
                </div>
                <table class="table table-striped">
                    <thead>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th>Puesto</th>
                    <th></th>
                    </thead>
                    <tbody>
                        <c:forEach items="${empleados}" var="empleado">
                            <tr>
                                <td><c:out value="${empleado.NOMBRES}"/></td>
                                <td><c:out value="${empleado.APELLIDOS}"/></td>
                                <td><c:out value="${empleado.PUESTO}"/></td>
                                <td>
                                    <a  href="<c:url value="empleados.htm"/>" class="btn btn-primary"><i class="glyphicon glyphicon-edit"></i></a>
                                    <a  href="<c:url value="empleados.htm"/>" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
