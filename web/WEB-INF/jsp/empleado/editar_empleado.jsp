<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nuevo Empleado</title>        
        <link rel="stylesheet" href="<c:url value="/node_modules/bootstrap/dist/css/bootstrap.min.css" />" >
        <link rel="stylesheet" href="<c:url value="/assets/css/Proyecto.css" />">
    </head>
    <body>
        <c:import url="../navbar/navbar.jsp" />
        <div class="container container-fluid container-top">
            <div class="row">
                <h1>Crear empleado</h1>
                <div>
                    <a href="<c:url value="empleados.htm"/>" class="btn btn-info"><i class="glyphicon glyphicon-chevron-left"></i> Regresar</a>
                </div>
                <form:form method="post" commandName="empleados">
                    <form:errors path="*" element="div" cssClass="alert alert-danger" />
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <!--<input type="text" class="form-control" id="nombre" placeholder="Nombre">-->
                        <form:input path="Nombre" cssClass="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="apellidos">Apellidos</label>
                        <input type="text" class="form-control" id="apellidos" placeholder="Apellidos">
                    </div>
                    <div class="form-group">
                        <select class="form-control">
                            <c:forEach items="${puestos}" var="puesto">
                                <option value="<c:out value="${puesto.PUESTO_ID}"/>"><c:out value="${puesto.NOMBRE}"/></option>
                            </c:forEach>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-default">Crear</button>
                </form:form>
            </div>
        </div>
    </body>
</html>
