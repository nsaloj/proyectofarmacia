<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Puestos</title>
        <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="<c:url value="/assets/css/Proyecto.css"/>">
    </head>
    <body>
        <c:import url="../navbar/navbar.jsp" />
        <div class="container container-top">
            <div class="row">
                <h1>Puestos</h1>
                <table class="table table-striped">
                    <thead>
                    <th>Nombre</th>
                    <th>Rango</th>
                    <th></th>
                    </thead>
                    <tbody>
                        <c:forEach items="${puestos}" var="puesto">
                            <tr>
                                <td><c:out value="${puesto.NOMBRE}"/></td>
                                <td><c:out value="${puesto.RANGO}"/></td>
                                <td></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
