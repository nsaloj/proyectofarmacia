<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Proyecto farmacia</title>
        <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="<c:url value="/assets/css/Proyecto.css" />" rel="stylesheet">
    </head>

    <body>
        <!--<h1>Esto es una prueba</h1>-->
    <c:import url="navbar/navbar.jsp" />
    
    <div class="page-container">
    </div>
    <script src="<c:url value="/assets/js/jquery-3.2.1.min.js" />"></script>
    <script src="<c:url value="/node_modules/bootstrap/dist/js/bootstrap.min.js" />"></script>
</body>
</html>
