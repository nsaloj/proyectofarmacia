<%@page contentType="text/html" pageEncoding="UTF-8"%>
<nav class="navbar navbar-fixed-top navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-sidebar="#sidebar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <!--<span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>-->
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="#">
                    Farmacia
                    <!--<img src="assets/images/isotipo-inv-bg.png" /> market
                    <span class="s-market">S</span>market-->
                </a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="#">Inicio</a></li>
                    <li><a href="inventario.htm">Inventario</a></li>
                    <li><a href="puestos.htm">Puestos</a></li>
                    <li><a href="empleados.htm">Empleados</a></li>
                    <li><a href="usuarios.htm">Usuarios</a></li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </nav>